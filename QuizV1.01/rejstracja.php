<?php

    session_start();

    if(isset($_POST['email']))
    {
        //Udana walidacja ? taK 
        $wszystko_OK=true;
        
        //Sprawdź poprawność adresu email
        $email = $_POST['email'];
        $emailB = filter_var($email,FILTER_SANITIZE_EMAIL);

        if((filter_var($emailB,FILTER_SANITIZE_EMAIL)==false) ||($emailB!=$email))
        {
            $wszystko_OK=false;
            $_SESSION['e_email']="Podaj poprawny adres email!";
        }
        //Sprawdź poprawność hasła
        $haslo1 = $_POST['haslo1'];
        $haslo2 = $_POST['haslo2'];

        if((strlen($haslo1)<8) || (strlen($haslo1)>20))
        {
            $wszystko_OK=false;
            $_SESSION['e_haslo']="Hasło musi posiadać od 8 do 20 znaków!";
        }
        if($haslo1!=$haslo2)
        {
            $wszystko_OK=false;
            $_SESSION['e_haslo']="Podane hasła nie są identyczne!";
        }
        //Hashowanie haseł
        $haslo_hash = password_hash($haslo1,PASSWORD_DEFAULT);

        //Sprawdzenie akceptacji regulaminu
        if(!isset($_POST['regulamin']))
        {
            $wszystko_OK=false;
            $_SESSION['e_box']="Regulamin nie został zaakcepotowany!";
        }

        //Bot or not? Oto jest questions!
        $sekret = "6Ld2QOIcAAAAAKFnGpJ0YGSDu-PBXevV0WZVWZ-j";
        
        $sprawdz = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$sekret.'&response='.$_POST['g-recaptcha-response']);

        $odpowiedz = json_decode($sprawdz);

        if(!($odpowiedz->success))
        {
            $wszystko_OK=false;
            $_SESSION['e_boty']="Potwierdź, że nie jesteś botem!";
        }

        //Zapamiętaj wpisaną date
        $_SESSION['fr_email']= $email;
        $_SESSION['fr_haslo1']= $haslo1;
        $_SESSION['fr_haslo2']= $haslo2;
        
        if(isset($_POST['regulamin'])) $_SESSION['fr_regulamin']=true;


        require_once "connect2.php";
        mysqli_report(MYSQLI_REPORT_STRICT);

        try
        {
            $polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
            if ($polaczenie->connect_errno!=0)
            {
                throw new Exception(mysqli_connect_errno());
            }
            else
            {
                //Czy email juz istnieje
                $rezultat = $polaczenie->query("SELECT id FROM `users` WHERE `email`='$email'");
                if(!$rezultat) throw new Exception($polaczenie->error);

                $ile_taich_maili = $rezultat->num_rows;
                if($ile_taich_maili>0)
                {
                    $wszystko_OK=false;
                    $_SESSION['e_email']="Istnieje już konto z tym e-mailem!";
                }
                if($wszystko_OK==true)
                {
                    // wszystkie testy zaliczone, dodajemy gracza do bazy
                    
                    if($polaczenie->query("INSERT INTO `users` VALUES(NULL,'$email','$haslo_hash')"))
                    {
                        $_SESSION['udanarejestracja']=true;
                        header('Location:logowanie.php');
                    }
                    else
                    {
                        throw new Exception($polaczenie->error);
                    }
                }
                $polaczenie->close();
            }
        }
        catch(Exception $e)
        {
            echo '<span style="color:red;">Błąd serwera! Przepraszamy za niedogodności i prosimy o rejestrację w innym terminie! naprawa soon! </span>';
            echo '<br/>Informacja developerska: '.$e;
        }
    }
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8" />
    <title>Quiz--rejstracja</title>
    <meta name="description" content="osadnicy"/>
    <meta name="keywords" content="osadnicy, Gra mmo" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <style>
        body
        {
            background-color: lightgrey;
        }
        .error
        {
            color: red;
            margin-top: 10px;
            margin-bottom: 10px;

        }
    </style>
</head>

<body>

    <form method="post">

        E-mail: <br/><input type="text" value="<?php
        if(isset($_SESSION['fr_email']))
        {
            echo $_SESSION['fr_email'];
            unset($_SESSION['fr_email']);
        }
        ?>" name="email"/><br/>
        <?php
            if(isset($_SESSION['e_email']))
            {
                echo '<div class="error">'.$_SESSION['e_email'].'</div>';
                unset($_SESSION['e_email']);
            }
        ?>
        Hasło: <br/><input type="password" value="<?php
        if(isset($_SESSION['fr_haslo1']))
        {
            echo $_SESSION['fr_haslo1'];
            unset($_SESSION['fr_haslo1']);

        }
        ?>" name="haslo1"/><br/>
        <?php
            if(isset($_SESSION['e_haslo']))
            {
                echo '<div class="error">'.$_SESSION['e_haslo'].'</div>';
                unset($_SESSION['e_haslo']);
            }
        ?>
        Powtóż hasło: <br/><input type="password" name="haslo2"/><br/>
        <label>
        <input type="checkbox" name="regulamin"<?php
        if(isset($_SESSION['fr_box']))
        {
            echo "checked";
            unset($_SESSION['fr_box']);
        }
            ?>/> Akceptuję regulamin
        </label>
        <?php
            if(isset($_SESSION['e_box']))
            {
                echo '<div class="error">'.$_SESSION['e_box'].'</div>';
                unset($_SESSION['e_box']);
            }
        ?>
        <div class="g-recaptcha" data-sitekey="6Ld2QOIcAAAAAHjaxFBkshSNcVPOtLtlZqkStRJD"></div>
        <?php
            if(isset($_SESSION['e_boty']))
            {
                echo '<div class="error">'.$_SESSION['e_boty'].'</div>';
                unset($_SESSION['e_boty']);
            }
        ?>
        <br /><br />
        <input type="submit" value="Zarejestruj się" />

    </form>
</body>
</html>
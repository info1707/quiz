-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 31 Sty 2022, 23:25
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `shop`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--

CREATE TABLE `pytania` (
  `id` int(10) UNSIGNED NOT NULL,
  `pytanie` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpA` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpB` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpC` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpD` text COLLATE utf8mb4_polish_ci NOT NULL,
  `prawidlowa_odp` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odp` text COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`id`, `pytanie`, `odpA`, `odpB`, `odpC`, `odpD`, `prawidlowa_odp`, `odp`) VALUES
(1, 'Program zapisany w języku PHP ma za zadanie obliczyć średnią pozytywnych ocen ucznia od 2 do 6. \r\nWarunek wybierania ocen w pętli liczącej średnią powinien zawierać wyrażenie logiczne\r\n', '$ocena > 2 and $ocena < 6', '$ocena > 2 or $ocena < ', '$ocena >= 2 or $ocena <= 6', '$ocena >= 2 and $ocena <= ', 'd', 'a'),
(2, 'W jaki sposób, stosując język PHP, zapisać w ciasteczku napis znajdujący się w zmiennej dane na czas \r\njednego dnia?', 'setcookie(\"dane\", $dane, 0);', ' setcookie(\"dane\", \"dane\", 0);', ' setcookie(\"dane\", $dane, time());\r\n', ' setcookie(\"dane\", $dane, time() + (3600*24)); ', 'd', 'a'),
(3, 'if (empty($_POST[\"name\"])) {\r\n$nameErr = \"Name is required\";\r\n} \r\nPrzedstawiony fragment kodu PHP służy do obsługi\r\n', 'sesji.', 'ciasteczek.', 'formularza.', 'bazy danych.', 'c', 'a'),
(4, 'echo date(\"Y\");\r\nPo wykonaniu kodu PHP zostanie wyświetlona aktualna data zawierająca jedynie', 'rok.\r\n', 'dzień.\r\n', 'miesiąc.', 'Żadna ze wskazanych.', 'a', 'd'),
(5, 'Który zapis definiuje w języku PHP komentarz wieloliniowy?\n', '#', '//', '/* */', '<!-- -->', 'c', 'd'),
(6, 'Integralność encji w bazie danych zostanie zachowana, jeżeli między innymi\n', 'klucz główny będzie zawsze liczbą całkowitą.\r\n', ' każdej kolumnie zostanie przypisany typ danych.', 'dla każdej tabeli zostanie utworzony klucz główny', 'każdy klucz główny będzie miał odpowiadający mu klucz obcy w innej tabeli.', 'c', 'd'),
(7, 'Aby przy pomocy zapytania SQL zmodyfikować strukturę istniejącej tabeli, należy zastosować kwerendę\r\n', 'UPDATE', 'INSERT INTO\r\n', 'ALTER TABLE\r\n', 'CREATE TABLE\r\n', 'c', 'd'),
(8, 'SELECT AVG(cena) FROM uslugi;\r\nFunkcja agregująca AVG użyta w zapytaniu ma za zadanie\r\n', 'zsumować koszt wszystkich usług.', 'wskazać najwyższą cenę za usługi.', 'policzyć ile jest usług dostępnych w tabeli.', 'obliczyć średnią arytmetyczną cen wszystkich usług.', 'd', 'd'),
(9, 'SELECT imie FROM mieszkancy WHERE imie LIKE \'_r%\';\nKtóre imiona zastosowaną wybrane w wyniku tego zapytania?\n', 'Krzysztof, Krystyna, Romuald', 'Rafał, Rebeka, Renata, Roksana.', 'Gerald, Jarosław, Marek, Tamara.', 'Arleta, Krzysztof, Krystyna, Tristan.', 'd', 'd'),
(10, 'Kwerendę SELECT DISTINCT należy zastosować w przypadku, gdy potrzeba wybrać rekordy', ' pogrupowane. ', 'występujące w bazie tylko raz.\r\n', 'posortowane malejąco lub rosnąco.', 'tak, aby w podanej kolumnie nie powtarzały się wartości.', 'd', 'd'),
(11, 'Którego typu danych w bazie MySQL należy użyć, aby przechować w jednym polu datę i czas?', 'DATE', 'YEAR', 'BOOLEAN\r\n', 'TIMESTAMP', 'd', 'd'),
(12, 'Aby edytować dane w bazie danych można posłużyć się', 'raportem.', ' formularzem.\r\n', 'filtrowaniem.\r\n', 'kwerendą SELECT.', 'b', 'd'),
(13, 'Aby usunąć wszystkie rekordy z tabeli należy zastosować kwerendę', 'INSERT INTO\r\n', 'ALTER COLUMN', 'CREATE COLUMN', 'TRUNCATE TABLE', 'b', 'd'),
(14, 'ALTER TABLE artykuly MODIFY cena float;\r\nKwerenda ma za zadanie w tabeli artykuly', 'usunąć kolumnę cena typu float.\r\n', 'zmienić typ na float dla kolumny cena.', 'zmienić nazwę kolumny z cena na float.', 'dodać kolumnę cena o typie float, jeśli nie istnieje.', 'b', 'd'),
(15, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n', 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'd', 'd'),
(16, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do', 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.', 'a', 'd'),
(17, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n', 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'd', 'd'),
(18, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do', 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.', 'a', 'd'),
(19, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n', 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'd', 'd'),
(20, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do', 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.', 'a', 'd'),
(21, 'Pole insert_id zdefiniowane w bibliotece MySQLi języka PHP może być wykorzystane do ', 'otrzymania id ostatnio wstawionego wiersza.', 'otrzymania kodu błędu, gdy proces wstawiania wiersza się nie powiódł.', 'pobrania najwyższego indeksu bazy, aby po jego inkrementacji wstawić pod niego dane.\r\n', 'pobrania pierwszego wolnego indeksu bazy, tak, aby można było pod nim wstawić nowe dane.', 'a', 'd'),
(22, 'Znaczniki HTML <strong> oraz <em> służące do podkreślenia ważności tekstu, pod względem formatowania \r\nsą odpowiednikami znaczników', ' <i> oraz <mark>', ' <u> oraz <sup>', ' <b> oraz <i>', '<b> oraz <u>', 'c', 'c'),
(23, 'W języku CSS, należy zdefiniować tło dokumentu jako obraz rys.png. Obraz ma powtarzać się jedynie \r\nw poziomie. Którą definicję należy przypisać selektorowi body?', '{background-image: url(\"rys.png\"); background-repeat: round;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat-x;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat-y;}\r\n', 'c', 'd'),
(24, 'W języku CSS zapis selektora p > i { color: red;} oznacza, że kolorem czerwonym zostanie \r\nsformatowany', 'każdy tekst w znaczniku <p> lub każdy tekst w znaczniku <i>', 'każdy tekst w znaczniku <p> za wyjątkiem tych w znaczniku <i>\r\n', 'jedynie ten tekst znacznika <p>, do którego jest przypisana klasa o nazwie i\r\n', 'jedynie ten tekst w znaczniku <i>, który jest umieszczony bezpośrednio wewnątrz znacznika <p>', 'd', 'd'),
(25, 'input:focus { background-color: LightGreen; }\r\nW języku CSS zdefiniowano formatowanie dla pola edycyjnego. Tak formatowane pole edycyjne będzie miało \r\njasnozielone tło ', 'jeśli jest to pierwsze wystąpienie tego znacznika w dokumencie.\r\n', 'gdy zostanie wskazane kursorem myszy bez kliknięcia.', 'po kliknięciu myszą w celu zapisania w nim tekstu.', 'w każdym przypadku.', 'c', 'd'),
(26, 'Kolorem o barwie niebieskiej jest kolor', '#0000EE', '#EE0000', '#00EE00', '#EE00EE\r\n', 'a', 'd'),
(27, 'Którym poleceniem można wyświetlić konfigurację serwera PHP, w tym informację m. in. o: wersji PHP, \r\nsystemie operacyjnym serwera, wartości przedefiniowanych zmiennych?', 'echo(ini_get_all());', 'echo phpversion();\r\n', ' phpcredits();', ' phpinfo();\r\n', 'd', 'c'),
(28, 'Za pomocą którego słowa kluczowego deklaruje się zmienną w języku JavaScript?\r\n', 'let', 'new', '$', 'begin', 'a', 'd'),
(29, 'Pole lub zbiór pól jednoznacznie identyfikujący każdy pojedynczy wiersz w tabeli w bazie danych to klucz', 'inkrementacyjny.\r\n', 'podstawowy.', ' przestawny.', 'obcy.\r\n', 'b', 'd'),
(30, 'W języku SQL, aby zmienić strukturę tabeli, np. poprzez dodanie lub usunięcie kolumny, należy zastosować \r\npolecenie', 'UPDATE', 'TRUNCATE\r\n', 'DROP TABLE', 'ALTER TABLE', 'd', 'd'),
(31, 'Atrybut kolumny NOT NULL jest wymagany w przypadku', 'klucza podstawowego.', 'użycia atrybutu DEFAULT.\r\n', 'definicji wszystkich pól tabeli.', 'definicji wszystkich pól typu numerycznego.', 'a', 'd'),
(32, 'W bazach danych do prezentacji danych spełniających określone warunki nalezy utworzyć', 'raport.', 'relację.', 'formularz.', 'makropolecenie.', 'd', 'd'),
(33, 'Wskaż różnicę pomiędzy poleceniami DROP TABLE i TRUNCATE TABLE.\r\n', 'DROP TABLE usuwa tabelę, a TRUNCATE TABLE modyfikuje w niej dane spełniające \r\nwarunek.\r\n', ' DROP TABLE usuwa tabelę, a TRUNCATE TABLE usuwa wszystkie dane, pozostawiając \r\npustą tabelę.\r\n', 'Obydwa polecenia usuwają jedynie zawartość tabeli, ale tylko polecenie DROP TABLE może \r\nbyć cofnięte.\r\n', 'Obydwa polecenia usuwają tabelę wraz zawartością, ale tylko polecenie TRUNCATE TABLE \r\nmoże być cofnięte', 'b', 'd'),
(34, 'Aby nadać użytkownikowi uprawnienia do tabel w bazie danych, należy zastosować polecenie ', 'GRANT', 'SELECT ', 'CREATE', 'ALTER', 'a', 'd'),
(35, 'Aby przesłać dane za pomocą funkcji mysqli_query() w skrypcie PHP, który wstawia do bazy danych dane \r\npobrane z formularza ze strony internetowej, jako jednego z parametrów należy użyć kwerendy\r\n', 'INSERT INTO', 'UPDATE\r\n', 'SELECT', 'ALTER', 'a', 'c'),
(36, 'Który znacznik należy do znaczników definiujących listy w języku HTML?', '<tr>', '<ul>', '<td>', '<br>', 'b', 'd'),
(37, 'Której właściwości CSS należy użyć, aby zdefiniować marginesy wewnętrzne dla elementu?', 'hight ', 'margin', 'padding', 'width', 'c', 'd'),
(38, 'Globalne tablice do przechowywania danych o ciastkach i sesjach: $_COOKIE oraz $_SESSION są częścią \r\njęzyka', 'C#\r\n', 'Perl', 'PHP\r\n', 'JavaScript', 'c', 'd');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `ID` int(10) NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`ID`, `email`, `password`) VALUES
(1, 'Damian@wp.pl', '$2y$10$98ktpchm1EUaBqULqVZu8eImuemy3IfymZUkGV4OqwfWDDhviKb0G'),
(2, 'Andrzej@gmail.com', '$2y$10$nXHNYMsMFTN8.0..3V0Jo.9VTGv4uk1KmFS1pWZCrrPgEM0VD4aOa'),
(3, 'Jannowak1@gmail.com', '$2y$10$WRM/7.Q2DZVEduWVSm1OteLCKin11.RU4A4ZvR1phMYB/5xFzNp2.'),
(4, 'Domino2137@wp.pl', '$2y$10$JF/kt9OMyF4GoAdEqs5YqO9KmRujxmvpvSFgkM7j6YAioYArh0liO');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `pytania`
--
ALTER TABLE `pytania`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `pytania`
--
ALTER TABLE `pytania`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=377;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

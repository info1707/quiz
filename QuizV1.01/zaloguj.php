<?php

    //rozpoczęcie sesji
    session_start();

    // sprawdzenie czy jest ustawiony email i hasło
    if ((!isset($_POST['Email'])) || (!isset($_POST['Haslo'])))
    {
        header('Location: index.php');
        exit();
    }

    //odwołanie do pliku connect2.php
    require_once "connect2.php";
    mysqli_report(MYSQLI_REPORT_STRICT);

    try
    {
        $polaczenie = new mysqli($host, $db_user, $db_password, $db_name);

        //sprawdzenie poprawnosci połączenia
        if ($polaczenie->connect_errno!=0) throw new Exception(mysqli_connect_errno());
        {
            $login = $_POST['Email'];
            $haslo = $_POST['Haslo'];

            //zmiana groźnych znaków html na ich odpowiedniki w enchach html'a
            $login = htmlentities($login, ENT_QUOTES, "UTF-8");
        }
        //zapytanie do bazydanych
        $rezultat = $polaczenie->query(sprintf("SELECT * FROM `users` WHERE `email`='%s'",
        mysqli_real_escape_string($polaczenie,$login)));
        //sprawdzenie poprawnosci połączenia i wyrzucenie ewentualnego błędu
        if(!$rezultat) throw new Exception($polaczenie->error);
        {
            //sprawdzenie czy istnieją podane wpisy w bazie danych
            $ilu_userow = $rezultat->num_rows;
            if($ilu_userow>0)
            {   
            {
                //odczytywanie bazy danych asocjacyjnie
                $wiersz = $rezultat->fetch_assoc();
                if(password_verify($haslo,$wiersz['password']))
                {
                    //ustawienie zmiennej potwierdzającej zalogowanie
                    $_SESSION['zalogowany'] = true;
                    $_SESSION['email'] = $wiersz['email'];

                    //usunięcie błędu gdyż już jest niepotrzebny
                    unset($_SESSION['blad']);
                    //uwolnienie zapytania bazy
                    $rezultat->free_result();
                    //zmiana lokalizacji na siema.php
                    header('Location: siema.php');
                }
                else
                {
                    //ustawienie błędu
                    $_SESSION['blad'] = '<span style="color:red">Niepoprawny email lub hasło!</span';
                    header('Location: index.php');
                }
            } 
            }
            //ustawienie błędu
            else
            {
                $_SESSION['blad'] = '<span style="color:red">Niepoprawny email lub hasło!</span';
                header('Location: index.php');
            }
        }
        //zakończenie połączenia
        $polaczenie->close();
    }
    //złapanie błędu do zmiennej $er żeby nie wyświetlać użytkownikowi za dużo informacji
    catch(Exception $er)
    {
        //Powiadomienie użytkownika o błędzie
        echo '<span style="color:red;">Błąd serwera! Przepraszamy za niedogodności i prosimy o zalogowanie w innym terminie! naprawa soon! </span>';
        //ta linijka wyświetla nam złapany błąd ale tego nie chcemy pokazać użytkownikowi więc jest zakomentowane
        //echo '<br/>Informacja developerska: '.$er;
    }

?>

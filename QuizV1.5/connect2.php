<?php
    // Połączenie teraz tylko jeden plik connect.php
if(!isset($_SESSION)) 
    { 
        session_start();
    }
try
{
    //Już bez require teraz wszystko dzieje się w sessji
    if(isset($_POST['db_user']))
    {
    $_SESSION['host'] = $_POST['host'];
    $_SESSION['db_user'] = $_POST['db_user'];
    $_SESSION['db_password'] = $_POST['db_password'];
    $_SESSION['db_name'] = $_POST['db_name'];
    $_SESSION['log'] = true;
    $host = $_SESSION['host'];
    $db_user = $_SESSION['db_user'];
    $db_password = $_SESSION['db_password'];
    $db_name = $_SESSION['db_name'];
    }
    $connect = @new mysqli($_SESSION['host'], $_SESSION['db_user'], $_SESSION['db_password'], $_SESSION['db_name']);
    //Sprawdzenie połączenia z bazą
    if($connect->connect_errno!=0)
    {
        throw new Exception(mysqli_connect_error());
    }
    else if(isset($_POST['host']))
    {
        header('Location:login.php');
    }
}//złapanie błędów połączenia z bazą
catch(Exception $er)
{
    echo '<span style="color:red;">Błąd serwera! Przepraszamy za niedogodności i prosimy o zalogowanie w innym terminie! (naprawa soon!) albo zalogowałeś się na konto, które posiada niewystarczające uprawnienia </span>';
    //echo '<br/>Informacja developerska: '.$er;
    session_unset();
}
?>
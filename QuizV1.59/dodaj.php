<?php
    if(!isset($_SESSION))
    {
        session_start();
    }
    if((!isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']!=true))
    {
        header('Location: login.php');
        exit();
    }
if(isset($_POST['Pytanie']))
{
    $connect = new mysqli($_SESSION['host'], $_SESSION['db_user'], $_SESSION['db_password'], $_SESSION['db_name']);
    $Quest = $_POST['Pytanie'];
    $Quest = htmlentities($Quest,ENT_QUOTES, "UTF-8");
    $AnsA  = $_POST['OdpA'];
    $AnsA = htmlentities($AnsA,ENT_QUOTES, "UTF-8");
    $AnsB  = $_POST['OdpB'];
    $AnsB = htmlentities($AnsB,ENT_QUOTES, "UTF-8");
    $AnsC  = $_POST['OdpC'];
    $AnsC = htmlentities($AnsC,ENT_QUOTES, "UTF-8");
    $AnsD  = $_POST['OdpD'];
    $AnsD = htmlentities($AnsD,ENT_QUOTES, "UTF-8");
    $TrueAns = $_POST['prawidlowa_odp'];
    $TrueAns = htmlentities($TrueAns,ENT_QUOTES, "UTF-8");

    $tablename = $_SESSION['table_name'];
    $quizkat = $_SESSION['Quiz_category'];

    $result = $connect->query("INSERT INTO `$tablename`(`id`, `pytanie`, `odpA`, `odpB`, `odpC`, `odpD`, `prawidlowa_odp`, `odp`) VALUES(NULL,'$Quest','$AnsA','$AnsB','$AnsC','$AnsD','$TrueAns',NULL)");
}
?>
<html>
    <head>
    <html lang="pl">
<title>Mój quiz</title>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
<body>
<div class="title">TWORZENIE QUIZU</div>
<?php
if(isset($_POST['table_name']) && (isset($_POST['Quiz_add'])))
{
    $_SESSION['Quiz_category'] = $_POST['Quiz_category'];
    $quizname = $_POST['table_name'];
    $quizkat = $_POST['Quiz_category'];
    $email = $_SESSION['email'];
    $tablename = $email.'_'.$quizkat.'_'.$quizname;
    $_SESSION['table_name'] = $tablename;

    $connect = new mysqli($_SESSION['host'], $_SESSION['db_user'], $_SESSION['db_password'], $_SESSION['db_name']);
    $result2 = $connect->query("CREATE TABLE `shop`.`$tablename` ( `ID` INT(3) NOT NULL AUTO_INCREMENT , `pytanie` TEXT NOT NULL , `odpA` TEXT NOT NULL , `odpB` TEXT NOT NULL , `odpC` TEXT NOT NULL , `odpD` TEXT NOT NULL , `prawidlowa_odp` TEXT NOT NULL , `odp` TEXT NULL , PRIMARY KEY (`ID`)) ENGINE = InnoDB;");
}
else if(isset($_POST['Dodaj']))
{
?>
<form action="" method="post">
<div class="choose">
    Wpisz nazwę Quizu: &nbsp;<input required type="text" name="table_name"></br>
    Wybierz kategorię Quizu: &nbsp;<input required type="text" name="Quiz_category">
    <input class="button3" type="submit" value="Utwórz Quiz" name="Quiz_add"/>
</form>
<?php
}

if(isset($_SESSION['Nrpyt_add']))
    {
    $NRpyt_add = $_SESSION['Nrpyt_add'];?>
    <form action="" method="post">
    <div id="pole_dodania_pytan">
        <br>
        Nr pytania: <?php echo '<span>'.$NRpyt_add.'</span>' ?><br><br>
        Pytanie: &nbsp;<input required type="text" name="Pytanie"><br><br>
        OdpA: &nbsp;<input required type="text" name="OdpA"><br><br>
        OdpB: &nbsp;<input required type="text" name="OdpB"><br><br>
        OdpC: &nbsp;<input required type="text" name="OdpC"><br><br>
        OdpD: &nbsp;<input required type="text" name="OdpD"><br><br>
        Prawidłowa odpowiedz: &nbsp;
        <input required type="radio" value="a" name="prawidlowa_odp">A
        <input required type="radio" value="b" name="prawidlowa_odp">B
        <input required type="radio" value="c" name="prawidlowa_odp">C
        <input required type="radio" value="d" name="prawidlowa_odp">D<br><br>
        <input class="button3" type="submit" name="Nrpyt_add" value="Wprowadź pytanie"/>
    </form>
    <form action="dodaj2.php" method="post">
    <input class="button3" type="submit" name="Quest_end" value="Zakończ tworzenie quizu">
    </div>
<?php
    $_SESSION['Nrpyt_add']++;
    }
else
{
    $_SESSION['Nrpyt_add'] = 1;
};
?>
</body>
</html>

<?php

    if(!isset($_SESSION))
    {
        session_start();
    }
    //Usunięcie danych z sessji
    session_unset();
    //powrót do strony z logowaniem
    header('Location: index.php');
    exit();
?>

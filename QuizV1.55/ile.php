<?php

    if(!isset($_SESSION)) 
    { 
        session_start();
    }
    if((!isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']!=true))
    {
      header('Location: login.php');
      exit();
    }
    if(isset($_GET['ile']))
    {
        unset($_GET['ile']);
        header('Location: ile.php');
        exit();
    }
?>
<html>
<head>
<title>Pytania</title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php
if(isset($_GET['start']))
{
  ?>
  <div class="title">Wybierz ile pytań ma mieć quiz:</div>
  <div class="radio-toolbar">
  <form class="wybor" action="siema.php" method="get">
      <input id="ile1" required type="radio" name="ile" value="5">
      <label for="ile1">5 pytań</label>
      <input id="ile2" required type="radio" name="ile" value="10">
      <label for="ile2">10 pytań</label>
      <input id="ile3" required type="radio" name="ile" value="20">
      <label for="ile3">20 pytań</label>
      <input id="ile4" required type="radio" name="ile" value="38">
      <label for="ile4">38 pytań(max)</label>
      </br>
      <button class="button3" name="start">Zatwierdź</button>
      </div>
  </form>
<?php
}
else
{
    ?>
    <div class="bump"><br><form action="ile.php" method="get">
    <button class="button" name="start" float="left"><span>START QUIZ</span></button>
    </form></div><br>
    <form action="dodaj.php" method="post">
    <button class="dodaj" name="Dodaj"><span>Dodaj pytanie do quizu</span></button>
    </form>
    <?php
}
?>
</body>
</html>

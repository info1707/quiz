<?php
    //Sprawdzenie czy użytkownik jest zalogowany oraz czy rozpoczął quiz
    if(!isset($_SESSION)) 
        { 
            session_start();
        }
    if((!isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']!=true))
        {
            header('Location: login.php');
            exit();
        }
    if(!isset($_POST['koniec']))
        {
            header('Location: ile.php');
            exit();
        }

?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WYNIK</title>
    <link href="style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <?php
    try
    {
        //Połączenie z bazą i wyrzucenie ewntualnego błędu
        $connect = new mysqli($_SESSION['host'], $_SESSION['db_user'], $_SESSION['db_password'], $_SESSION['db_name']);
        if($connect->connect_errno!=0)throw new Exception(mysqli_connect_errno());
        $maxpytan = @$_SESSION['clicks']-1;
        $score = @$_SESSION['score'];
    ?>
        <div class="title">Gratuluję ukończenia Quizu!!! użytkowniku&nbsp;<?php echo $gz = @$_SESSION['db_user']
        ?></div>
        <div class="wynik">Prawidłowych odpowiedzi:&nbsp;<?php echo $score .'/'.$maxpytan ;?> </div>
        <div class="procent">Uzyskałeś&nbsp;<?php $procent = $score/$maxpytan*100;
        echo round($procent,2).'%';?></div>


    <div class="podsumowanie">
        Poprawne odpowiedzi:
<?php
        $i=0;
        while($maxpytan>$i)
        {
            $i++;
            $qry = $connect->query(sprintf("SELECT * FROM `pytania` where id='%s';",mysqli_real_escape_string($connect,$i)));
            $num = mysqli_num_rows($qry);
            $row = mysqli_fetch_array($qry,MYSQLI_ASSOC);
            if(!$qry) throw new Exception($connect->error);?>
            <table class="odpy2">
            <tr><td><h2>Pytanie Nr.&nbsp;<?php echo htmlspecialchars($row['id']);?></h2></td></tr>
            <tr><td><h3><br/><?php echo htmlspecialchars($row['pytanie']);?></h3></td></tr>
            <?php 
            if($maxpytan >= 0)
                { ?>
                <tr><td class="<?php
                    $qry3 = $connect->query(sprintf("SELECT `id`, `prawidlowa_odp`, `odp` FROM `pytania` WHERE id='%s' ;",mysqli_real_escape_string($connect,$i)));
                        if(!$qry3) throw new Exception($connect->error);
                        ($row3 = mysqli_fetch_array($qry3, MYSQLI_ASSOC));
                    if($row3['prawidlowa_odp']=="a")
                        {
                            echo "wynikpoz";
                        }
                    else if($row3['prawidlowa_odp']!="a" && $row3['odp']=="a")
                        {
                            echo "wynikuser";
                        }
                    else
                        {
                            echo "wynikneg";
                        }
                   ?>"><div id="pyt1" name="odp" value="a">
                <?php echo htmlspecialchars($row['odpA']); ?></div></td></tr>
                <tr><td class="<?php
                    $qry3 = $connect->query(sprintf("SELECT `id`, `prawidlowa_odp`, `odp` FROM `pytania` WHERE id='%s' ;",mysqli_real_escape_string($connect,$i)));
                        if(!$qry3) throw new Exception($connect->error);
                        ($row3 = mysqli_fetch_array($qry3, MYSQLI_ASSOC));
                    if($row3['prawidlowa_odp']=="b")
                        {
                            echo "wynikpoz";
                        }
                    else if($row3['prawidlowa_odp']!="b" && $row3['odp']=="b")
                        {
                            echo "wynikuser";
                        }
                    else
                        {
                            echo "wynikneg";
                        }
                   ?>"><div id="pyt2" name="odp" value="b">
                <?php echo htmlspecialchars($row['odpB']); ?></div></td></tr>
                <tr><td class="<?php
                    $qry3 = $connect->query(sprintf("SELECT `id`, `prawidlowa_odp`, `odp` FROM `pytania` WHERE id='%s' ;",mysqli_real_escape_string($connect,$i)));
                        if(!$qry3) throw new Exception($connect->error);
                        ($row3 = mysqli_fetch_array($qry3, MYSQLI_ASSOC));
                    if($row3['prawidlowa_odp']=="c")
                        {
                            echo "wynikpoz";
                        }
                    else if($row3['prawidlowa_odp']!="c" && $row3['odp']=="c")
                        {
                            echo "wynikuser";
                        }
                    else
                        {
                            echo "wynikneg";
                        }
                   ?>"><div id="pyt3" name="odp" value="c">
                <?php echo htmlspecialchars($row['odpC']); ?></label></td></tr>
                <tr><td class="<?php
                    $qry3 = $connect->query(sprintf("SELECT `id`, `prawidlowa_odp`, `odp` FROM `pytania` WHERE id='%s' ;",mysqli_real_escape_string($connect,$i)));
                        if(!$qry3) throw new Exception($connect->error);
                        ($row3 = mysqli_fetch_array($qry3, MYSQLI_ASSOC));
                    if($row3['prawidlowa_odp']=="d" )
                        {
                            echo "wynikpoz";
                        }
                    else if($row3['prawidlowa_odp']!="d" && $row3['odp']=="d")
                        {
                            echo "wynikuser";
                        }
                    else
                        {
                            echo "wynikneg";
                        }
                   ?>"><div id="pyt4" name="odp" value="d">
                <?php echo htmlspecialchars($row['odpD']); ?></div></td></br></table>
                </tr><?php
                }
        }
    ?>

    </div>

<?php
    $qry4 = $connect->query("UPDATE `pytania` SET `odp` = NULL WHERE `pytania`.`id` > 0;");
    if(!$qry4) throw new Exception($connect->error);
    session_unset();
    }
    catch(Exception $er)
    {
        echo'<span style="color:red;">Błąd serwera!
        Przepraszamy za niedogodności (naprawa soon!) albo zalogowałeś się na konto w bazie danych które ma niewystarczające uprawnienia do używania quizu! </span>';
        //echo '<br/>Informacja developerska: '.$er;
        session_unset();
    }
    ?></div><br>
</body>
</html>

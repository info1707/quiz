<?php
    if(!isset($_SESSION))
    {
        session_start();
    }
    if((!isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']!=true))
    {
        header('Location: login.php');
        exit();
    }
if(isset($_POST['Pytanie']))
{
    $connect = new mysqli($_SESSION['host'], $_SESSION['db_user'], $_SESSION['db_password'], $_SESSION['db_name']);
    $Quest = $_POST['Pytanie'];
    $Quest = htmlentities($Quest,ENT_QUOTES, "UTF-8");
    $AnsA  = $_POST['OdpA'];
    $AnsA = htmlentities($AnsA,ENT_QUOTES, "UTF-8");
    $AnsB  = $_POST['OdpB'];
    $AnsB = htmlentities($AnsB,ENT_QUOTES, "UTF-8");
    $AnsC  = $_POST['OdpC'];
    $AnsC = htmlentities($AnsC,ENT_QUOTES, "UTF-8");
    $AnsD  = $_POST['OdpD'];
    $AnsD = htmlentities($AnsD,ENT_QUOTES, "UTF-8");
    $TrueAns = $_POST['prawidlowa_odp'];
    $TrueAns = htmlentities($TrueAns,ENT_QUOTES, "UTF-8");

    $result = $connect->query("INSERT INTO `pytania`(`id`, `pytanie`, `odpA`, `odpB`, `odpC`, `odpD`, `prawidlowa_odp`, `odp`) VALUES(NULL,'$Quest','$AnsA','$AnsB','$AnsC','$AnsD','$TrueAns',NULL)");
}
?>
<html>
    <head>
    <html lang="pl">
<title>Mój quiz</title>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
<body>

<form action="" method="post">
<div id="pole_dodania_pytan">
    <br>
    Pytanie: &nbsp;<input type="text" name="Pytanie"><br><br>
    OdpA: &nbsp;<input type="text" name="OdpA"><br><br>
    OdpB: &nbsp;<input type="text" name="OdpB"><br><br>
    OdpC: &nbsp;<input type="text" name="OdpC"><br><br>
    OdpD: &nbsp;<input type="text" name="OdpD"><br><br>
    Prawidłowa odpowiedz: &nbsp;
    <input type="radio" value="a" name="prawidlowa_odp">A
    <input type="radio" value="b" name="prawidlowa_odp">B
    <input type="radio" value="c" name="prawidlowa_odp">C
    <input type="radio" value="d" name="prawidlowa_odp">D<br><br>
    <input class="button3" type="submit" value="Wprowadź pytanie"/>
</form>
</div>
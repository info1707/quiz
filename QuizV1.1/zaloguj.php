<?php

    session_start();

    if ((!isset($_POST['Email'])) || (!isset($_POST['Haslo'])))
    {
        header('Location: index.php');
        exit();
    }

    require_once "connect2.php";
    mysqli_report(MYSQLI_REPORT_STRICT);

    try
    {
        $polaczenie = new mysqli($host, $db_user, $db_password, $db_name);

        if ($polaczenie->connect_errno!=0) throw new Exception(mysqli_connect_errno());
        {
            $login = $_POST['Email'];
            $haslo = $_POST['Haslo'];
            $login = htmlentities($login, ENT_QUOTES, "UTF-8");
        }
        $rezultat = $polaczenie->query(sprintf("SELECT * FROM `users` WHERE `email`='%s'",
        mysqli_real_escape_string($polaczenie,$login)));
        if(!$rezultat) throw new Exception($polaczenie->error);
        {
            $ilu_userow = $rezultat->num_rows;
            if($ilu_userow>0)
            {   
                $wiersz = $rezultat->fetch_assoc();
                if(password_verify($haslo,$wiersz['password']))
                {
                    $_SESSION['zalogowany'] = true;
                    $_SESSION['email'] = $wiersz['email'];

                    unset($_SESSION['blad']);
                    $rezultat->free_result();
                    header('Location: ile.php');
                }
                else
                {
                    $_SESSION['blad'] = '<span style="color:red">Niepoprawny email lub hasło!</span';
                    header('Location: index.php');
                }
            }
            else
            {
                $_SESSION['blad'] = '<span style="color:red">Niepoprawny email lub hasło!</span';
                header('Location: index.php');
            }
        }
        $polaczenie->close();
    }
    catch(Exception $er)
    {
        echo '<span style="color:red;">Błąd serwera! Przepraszamy za niedogodności i prosimy o zalogowanie w innym terminie! naprawa soon! </span>';
        //echo '<br/>Informacja developerska: '.$er;
    }

?>